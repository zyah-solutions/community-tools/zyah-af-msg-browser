# Bowzer the Browser Changelog

### v1.3.0 (2022/??/??)
- Improved experience when searching for a payload method Msg's callers (on the BD vs the project).

### v1.2.2 (2022/07/15)
- Added glyph support for community scoped messages.

### v1.2.1 (2022/07/07)
- Onscreen documentation update.
- Splash screen logo now incorporates the AF Guild.

### v1.2.0 (2022/04/19)
- Added minimum height to Bowzer.
- Added a splash screen.
- Added more example projects for comparison between QMH and AF.

### v1.1.1 (2022/03/15)
- Enabled full-speed scrolling in the Msg List (previously full-speed scrolling could cause an error that shut down Bowzer).

### v1.1.0 (2022/03/14)
- Dragging a payload method to another VI's block diagram now places the "Send" method of that Msg on the block diagram.

### v1.0.0 (2022/03/12)
- Initial public release

# Known Issues
- Scrolling too quickly between payload methods on some computers can cause an error that shuts down Bowzer.
- The target of the selected project item determines the application instance of Bowzer which can lead to strange behaviors on cRIOs and other Linux RT targets.